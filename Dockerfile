FROM alpine:3.15

# ideally, this version of caldera supports MCP agents only
#          we need to decide if we want to support concurrent MCP and Caldera (Sandcat/Manx) agents
#          during the same activity

# choosing alpine 3.15 (python 3.9) for compatibility with caldera

# python 3.10 (in ubuntu 22.04 and alpine 3.16 has asyncio deprications)

# TODO: Migrate to UBI (note: UBI uses py3.6 which is missing dependencies)
# FROM registry.access.redhat.com/ubi8/ubi:8.6-855
# dnf install -y libffi-devel openssl-devel python3-cryptography python3-pip python3-cffi golang haproxy


# note: adding all packages from distro where requirements.txt doesn't specify an exact version
#       or if the version in the repo matches the version in the requirements
#
RUN apk update && apk upgrade && \
    apk add python3 go py3-pip py3-wheel python3-dev py3-cffi \
    haproxy musl-dev libffi-dev dumb-init \
    py3-lxml py3-cryptography py3-yaml py3-pillow py3-babel py3-asn1 py3-asn1-modules py3-markupsafe \
    py3-frozenlist py3-multidict py3-commonmark py3-pygments py3-tinycss2 py3-alabaster py3-commonmark \
    py3-aiosignal py3-async-timeout py3-yarl py3-websocket-client py3-snowballstemmer py3-pexpect py3-attrs \
    py3-aiohttp==3.8.1-r0 py3-docutils==0.16-r3 py3-reportlab py3-reportlab minizip

# 3.16 will need patched as it removed the Callable from collections
#RUN echo "ALPINE 3.16 fix - shimming collections.abc.Callable as collections.Callable" 1>&2 && \
#    sed -i '39 a from collections.abc import Callable' /usr/lib/python3.10/collections/__init__.py

WORKDIR /usr/src/app
ADD . /usr/src/app

# RUN adduser -D caldera
# USER caldera
# remove dev venv (if exists)
RUN rm -rf venv


RUN pip3 install -r requirements.txt && \
    pip3 install --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain  && \
    pip3 install -r requirements-new.txt

RUN apk del python3-dev py3-wheel gcc musl-dev libffi-dev

# TODO: Brain library protobufs are out of date and only work with pure python implementation
ENV PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python

ENTRYPOINT  ["/usr/bin/dumb-init", "--"]
# order matters here, MCPTTPs must load before mcp/etc
CMD ["python3", "server.py", "--insecure"]
