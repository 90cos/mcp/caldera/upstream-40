"""
includes some packers.

For now, each pack_* function is generally self contained.
This will lead to a lot of duplicate code.
Need to sort out the integration mechanics with caldera first.

"""


import os
import pexpect
from random import choice
from tempfile import TemporaryDirectory
from multidict import CIMultiDict, CIMultiDictProxy
from app.utility.base_service import BaseService


class C2Service(BaseService):

    def __init__(self, services):
        self.file_svc = services.get('file_svc')
        self.data_svc = services.get('data_svc')
        self.contact_svc = services.get('contact_svc')
        self.app_svc = services.get('app_svc')
        self.log = self.create_logger('mcpc2_svc')
        self.pack_dir = os.path.relpath(os.path.join('plugins', 'mcpplugins'))
        self.log.debug("mcpC2 initialized")
